package jgile2.mods.bcplugins.blocks;

import jgile2.mods.bcplugins.bcplugins;
import jgile2.mods.bcplugins.TileEntity.TileCharger;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BlockCharger extends BlockContainer{

	public BlockCharger(int par1, Material par2Material) {
		super(par1, par2Material);
		this.setCreativeTab(CreativeTabs.tabRedstone);
		this.setHardness(3.5F);
		this.setStepSound(soundStoneFootstep);
		this.setUnlocalizedName("BlockCharger");
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileCharger();
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer ep, int par6, float par7, float par8, float par9) {
		if (!world.isRemote){
		ep.openGui(bcplugins.instance, bcplugins.guiIdCharger, world, x, y, z);
		//FMLClientHandler.instance().displayGuiScreen(ep, new guiCharger(ep,world,x,y,z));
		}
		return true;
	}
	
	@Override
	public void registerIcons(IconRegister ir){
		this.blockIcon = ir.registerIcon("bcplugins:plusstone_side");
	}
	
	/**
	 * The type of render function that is called for this block
	 */
	public int getRenderType()
	{
	return -1;
	}

	/**
	 * Is this block (a) opaque and (B) a full 1m cube? This determines whether or not to render the shared face of two
	 * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
	 */
	public boolean isOpaqueCube()
	{
		return false;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
	 */
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
	{
		int rotation = MathHelper.floor_double((double)((entityliving.rotationYaw * 4F) / 360F) + 2.5D) & 3;
		//world.setBlockMetadata(i, j, k, rotation - 1);
		world.setBlockMetadataWithNotify(i, j, k, rotation-1 , rotation);
	}
}
