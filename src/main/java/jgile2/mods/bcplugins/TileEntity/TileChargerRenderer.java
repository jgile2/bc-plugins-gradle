package jgile2.mods.bcplugins.TileEntity;

import jgile2.mods.bcplugins.client.modelCharger;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class TileChargerRenderer extends TileEntitySpecialRenderer 
{
	private modelCharger model; 

	public TileChargerRenderer() {
		model = new modelCharger();
	} 

	public void renderAModelAt(TileCharger tile, double d, double d1, double d2, float f) {
		
		int i = 0; 

		if (tile.worldObj != null) 
		{
			i = (tile.worldObj.getBlockMetadata(tile.xCoord, tile.yCoord,
					tile.zCoord)); 
		}
			bindTexture(new ResourceLocation("bcplugins", "textures/blocks/charger.png")); 

		GL11.glPushMatrix(); // start
		GL11.glTranslatef((float) d + 0.5F, (float) d1 + 1.5F,
				(float) d2 + 0.5F); // size
		GL11.glRotatef(0, 0.0F, 1.0F, 0.0F); 
		GL11.glScalef(1.0F, -1F, -1F); 
		model.renderAll();		
		GL11.glPopMatrix(); // end

	}

	public void renderTileEntityAt(TileEntity tileentity, double d, double d1,
			double d2, float f) {
		renderAModelAt((TileCharger) tileentity, d, d1, d2, f); // where to
																		// render
	}

}