package jgile2.mods.bcplugins.TileEntity;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import jgile2.mods.bcplugins.PacketHandler;
import jgile2.mods.bcplugins.blocks.BlockCharger;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import buildcraft.api.gates.IAction;
import buildcraft.api.gates.IActionReceptor;
import buildcraft.api.power.IPowerEmitter;
import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerHandler;
import buildcraft.api.power.PowerHandler.PowerReceiver;
import buildcraft.api.power.PowerHandler.Type;
import buildcraft.core.IMachine;
import buildcraft.core.triggers.ActionMachineControl;

import com.google.common.io.ByteArrayDataInput;

import cpw.mods.fml.common.network.PacketDispatcher;

public class TileCharger extends TileEntity implements  IInventory,IPowerReceptor, IActionReceptor, IMachine {
	private PowerHandler powerHandler;
	private static int POWER_USAGE = 25;
	private ActionMachineControl.Mode lastMode = ActionMachineControl.Mode.Unknown;
	private ItemStack[] chestStuff = new ItemStack[1];
	private String stuffName;
	private String localizedName;
	public int playersCurrentlyUsingChest;
	public float power = 10;
	public int powerStored;
	public int number;
	public int send;
	


	
	public TileCharger(){
		powerHandler = new PowerHandler(this, Type.MACHINE);
		powerHandler.configurePowerPerdition(0, 0);
	}
	@Override
	public int getSizeInventory() {
		return 1;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return this.chestStuff[i];
	}

	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (this.chestStuff[i] != null)
        {
            ItemStack itemstack;

            if (this.chestStuff[i].stackSize <= j)
            {
                itemstack = this.chestStuff[i];
                this.chestStuff[i] = null;
                this.onInventoryChanged();
                return itemstack;
            }
            else
            {
                itemstack = this.chestStuff[i].splitStack(j);

                if (this.chestStuff[i].stackSize == 0)
                {
                    this.chestStuff[i] = null;
                }

                this.onInventoryChanged();
                return itemstack;
            }
        }
        else
        {
            return null;
        }

	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		  if (this.chestStuff[i] != null)
	        {
	            ItemStack itemstack = this.chestStuff[i];
	            this.chestStuff[i] = null;
	            return itemstack;
	        }
	        else {}

	        return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		 this.chestStuff[i] = itemstack;

	        if (itemstack != null && itemstack.stackSize > this.getInventoryStackLimit())
	        {
	            itemstack.stackSize = this.getInventoryStackLimit();
	        }

	        this.onInventoryChanged();
		
	}

	  public void writeToNBT(NBTTagCompound nbt)
	    {
	        super.writeToNBT(nbt);
	        NBTTagList nbttaglist = new NBTTagList();
	        for (int i = 0; i < this.chestStuff.length; i ++)
	        {
	            if (this.chestStuff[i] != null)
	            {
	                NBTTagCompound tag = new NBTTagCompound();
	                tag.setByte("Slot", (byte)i);
	                this.chestStuff[i].writeToNBT(tag);
	                nbttaglist.appendTag(tag);
	            }
	        }

	        nbt.setTag("Stuff", nbttaglist);

	        if (this.isInvNameLocalized())
	        {
	            nbt.setString("SutffName", this.stuffName);
	        }
	    }

	    public void readFromNBT(NBTTagCompound nbt)
	    {
	        super.readFromNBT(nbt);
	        NBTTagList nbttaglist = nbt.getTagList("Stuff");
	        this.chestStuff = new ItemStack[this.getSizeInventory()];
	        
	        for (int i = 0; i < nbttaglist.tagCount(); i ++)
	        {
	            NBTTagCompound nbtSlot = (NBTTagCompound) nbttaglist.tagAt(i);
	            int var1 = nbtSlot.getByte("Slot") & 255;

	            if (var1 >= 0  && var1 < this.chestStuff.length)
	            {
	                this.chestStuff[var1] = ItemStack.loadItemStackFromNBT(nbtSlot);
	            }
	        }

	        if (nbt.hasKey("StuffName"))
	        {
	            this.stuffName = nbt.getString("StuffName");
	        }
	    }
	@Override
	public String getInvName() {
		return this.isInvNameLocalized() ? this.localizedName : "Container.Charger";
	}

	@Override
	public boolean isInvNameLocalized() {
		return this.localizedName != null && this.localizedName.length() > 0;
	}

	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return true;
	}

	@Override
	public void openChest() {
		 if (this.playersCurrentlyUsingChest < 0)
	        {
	            this.playersCurrentlyUsingChest = 0;
	        }

	        this.playersCurrentlyUsingChest ++;
	        this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType().blockID, 1, this.playersCurrentlyUsingChest);
	        this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord, this.zCoord, this.getBlockType().blockID);
	        this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord - 1, this.zCoord, this.getBlockType().blockID);

	}

	@Override
	public void closeChest() {
		 if (this.getBlockType() != null && this.getBlockType() instanceof BlockCharger)
	        {
	            this.playersCurrentlyUsingChest --;
	            this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType().blockID, 1, this.playersCurrentlyUsingChest);
	            this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord, this.zCoord, this.getBlockType().blockID);
	            this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord - 1, this.zCoord, this.getBlockType().blockID);
	        }

		
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return true;
	}

	@Override
	public PowerReceiver getPowerReceiver(ForgeDirection side) {
		return powerHandler.getPowerReceiver();
	}

	@Override
	public void doWork(PowerHandler workProvider) {

		this.powerScaled(55);
		this.powerStored = (int)powerHandler.getEnergyStored();
		//System.out.println(powerScaled(55));
		repairItem();
	}
	
	 
    public int powerScaled(int i){
    	try{
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);
		
		//this.powerStored = (int)powerHandler.getEnergyStored();
    	float perc = this.powerStored/powerHandler.getMaxEnergyStored();
    	float before = perc*i;
    	send= (int)before;
    	dos.writeInt(send);
    	PacketDispatcher.sendPacketToAllAround(xCoord, xCoord, xCoord, 256, this.worldObj.provider.dimensionId, PacketHandler.composeTilePacket(bos));
    	//System.out.println(send);
    	
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	return send;

    }


	@Override
	public World getWorld() {
		return this.worldObj;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public boolean manageFluids() {
		return false;
	}

	@Override
	public boolean manageSolids() {
		
		return true;
	}

	@Override
	public boolean allowAction(IAction action) {

		return true;
	}

	@Override
	public void actionActivated(IAction action) {
		//this.lastMode = ActionMachineControl.Mode.On;
		
	}
	@Override
	public void invalidate() {
		super.invalidate();
	}
	
    @Override
    public void validate()
    {
    	super.validate();  	
    	powerHandler.configure(5, 100, 25, 5000);

    }
   
    @Override
    public void updateEntity(){
    	TileEntity te;
		for (ForgeDirection d : ForgeDirection.VALID_DIRECTIONS) {
			te = this.worldObj.getBlockTileEntity(this.xCoord + d.offsetX, this.yCoord + d.offsetY, this.zCoord + d.offsetZ);
			if (te instanceof IPowerEmitter) {
				PowerReceiver pr = ((IPowerReceptor) te).getPowerReceiver(d.getOpposite());
				if (pr != null) pr.receiveEnergy(Type.ENGINE, this.power, d.getOpposite());
			}
		}
    }
    
    public void repairItem(){
   
    	ItemStack currentItem = this.chestStuff[0];
    	
    	if(powerStored >25){
    		if (currentItem !=null&& currentItem.getItemDamage()<currentItem.getMaxDamage()){
    			currentItem.setItemDamage(currentItem.getItemDamage()-1);
    			powerHandler.useEnergy(25, 25, true);
    		}
    	}
    }

}
