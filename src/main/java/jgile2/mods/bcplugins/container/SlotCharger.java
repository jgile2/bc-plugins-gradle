package jgile2.mods.bcplugins.container;

import jgile2.mods.bcplugins.IChargeable;
import jgile2.mods.bcplugins.bcplugins;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotCharger extends Slot {

	public SlotCharger(IInventory par1iInventory, int par2, int par3, int par4) {
		super(par1iInventory, par2, par3, par4);
		// TODO Auto-generated constructor stub
	}
	
	@Override
    public boolean isItemValid(ItemStack par1ItemStack)
    {
		Item item = par1ItemStack.getItem();
		
		if(item instanceof IChargeable){
        return true;
		}
		return false;
    }


}
