package jgile2.mods.bcplugins.container;

import java.util.List;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;

import jgile2.mods.bcplugins.TileEntity.TileCharger;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class ContainerCharger extends Container
{
    private IInventory bottomPartOfChest;
    private int numberOfRows = 6;
    public  TileCharger chest;
	public int lastPower;
	public int currentPower;


    public ContainerCharger(EntityPlayer player, World world, int x, int y, int z)
    {
    	
        TileCharger chest = (TileCharger) world.getBlockTileEntity(x, y, z);
     
        this.bottomPartOfChest = chest;
        this.numberOfRows = chest.getSizeInventory() / 9;
        chest.openChest();
        int i = (this.numberOfRows - 4) * 18;
        int j;
        int row;

       this.addSlotToContainer(new SlotCharger(chest, 0, 80, 35));
                
        //Player Inventory
        for (j = 0; j < 3; j++)
        {
            for (row = 0; row < 9; row++)
            {
                this.addSlotToContainer(new Slot(player.inventory, row + j * 9 + 9, 8 + row * 18 , 156 + j * 18 + i));
            }
        }

        //Player Bottom Bar
        for (j = 0; j < 9; j ++)
        {
            this.addSlotToContainer(new Slot(player.inventory, j, 8 + j * 18 , 123 + 1 + 18));
        }
        
        currentPower = chest.powerStored;
    }

	@Override
	public void detectAndSendChanges() {
		 for (ICrafting c : (List <ICrafting>) crafters)
         {
                 if (c instanceof Player)
                 {
                         Packet250CustomPayload packet = new Packet250CustomPayload();

                         packet.channel = "bcplugins";
                         packet.length = 1;

                         byte[] bytes = new byte[1];

                         int energy = currentPower;
                      
                         bytes[0] = (byte) energy;
                       

                       
                         packet.data = bytes;
                         PacketDispatcher.sendPacketToPlayer(packet, (Player) c);
                 }
                 
                 
         }
         super.detectAndSendChanges();
 }


    public ItemStack transferStackInSlot(EntityPlayer player, int i)
    {
       return null;
    }

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		// TODO Auto-generated method stub
		return true;
	}

  
}
