package jgile2.mods.bcplugins.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import jgile2.mods.bcplugins.bcplugins;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.Icon;

public class reinforcedBlazeRod extends Item{

	public reinforcedBlazeRod(int par1) {
		super(par1);
		this.setUnlocalizedName("reinforcedBlazeRod");
		setCreativeTab(bcplugins.create);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister ir) {
		this.itemIcon = ir.registerIcon("bcplugins:reinforcedBlaze");
		
	}

}
